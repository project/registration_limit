<?php

declare(strict_types=1);

namespace Drupal\registration_limit\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure registration limit settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'registration_limit_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['registration_limit.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('registration_limit.settings');

    $form['ip_whitelist'] = [
      '#type' => 'textarea',
      '#title' => $this->t('IP Whitelist'),
      '#default_value' => $config->get('ip_whitelist'),
      '#description' => $this->t('IP addresses entered here are excluded from the IP check when registering a new account and can also create multiple accounts.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $ipWhitelistValue = $form_state->getValue('ip_whitelist');

    if (empty($ipWhitelistValue)) {
      return;
    }

    foreach (explode(PHP_EOL, $ipWhitelistValue) as $ipAddress) {
      if (!filter_var(trim($ipAddress), FILTER_VALIDATE_IP)) {
        $form_state->setErrorByName('ip_whitelist', sprintf('%s is not a valid IP address!', trim($ipAddress)));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('registration_limit.settings')
      ->set('ip_whitelist', $form_state->getValue('ip_whitelist'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
