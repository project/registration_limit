<?php

declare(strict_types=1);

namespace Drupal\registration_limit\Mapper;

use Drupal\Core\Database\Connection;
use Drupal\Core\Site\Settings;

/**
 * Mapper for last login table.
 */
class LastLoginMapper {

  /**
   * Construct new instance.
   */
  public function __construct(
    protected Connection $database,
  ) {}

  /**
   * Save data to table.
   *
   * @param string|int $uid
   *   The user id.
   * @param string $ip
   *   The ip address.
   */
  public function saveLastLoginData(string|int $uid, string $ip): void {
    $existingResult = $this->database->query(
      'SELECT uid FROM registration_limit_last_login WHERE uid = :uid',
      [
        ':uid' => $uid,
      ]
    )->fetchAssoc();

    // Update existing.
    if ($existingResult !== FALSE) {
      $this->database->query(
        'UPDATE registration_limit_last_login ' .
        'SET timestamp = :timestamp, ip = :ip WHERE uid = :uid',
        [
          ':uid' => $uid,
          ':timestamp' => time(),
          ':ip' => $ip,
        ]
      );
    }
    // Insert new.
    else {
      $this->database->query(
        'INSERT INTO registration_limit_last_login (uid, timestamp, ip) ' .
        'VALUES (:uid, :timestamp, :ip)',
        [
          ':uid' => $uid,
          ':timestamp' => time(),
          ':ip' => $ip,
        ]
      );
    }

  }

  /**
   * Checks if given IP address was used for a login within the last month.
   *
   * @param string $ip
   *   The ip address to check for.
   */
  public function ipRecentlyUsedForLogin(string $ip): bool {
    $limit = time() - $this->getLastLoginOffset();

    // @codingStandardsIgnoreStart
    $results = $this->database->query(
      'SELECT ip FROM registration_limit_last_login ' .
      'WHERE timestamp >= :limit ' .
      'AND ip = :ip',
      [
        ':limit' => $limit,
        ':ip' => $ip,
      ],
    )->fetchAll();
    // @codingStandardsIgnoreEnd

    return count($results) > 0;
  }

  /**
   * Checks if given IP address was used in a session.
   *
   * @param string $ip
   *   The ip address to check for.
   */
  public function ipRecentlyUsedInSession(string $ip): bool {
    // @codingStandardsIgnoreStart
    $results = $this->database->query(
      'SELECT uid FROM sessions ' .
      'WHERE hostname = :ip ' .
      'AND uid > 1',
      [
        ':ip' => $ip,
      ],
    )->fetchAll();
    // @codingStandardsIgnoreEnd

    return count($results) > 0;
  }

  /**
   * Get last login offset from settings.
   */
  protected function getLastLoginOffset(): int {
    return Settings::get('registration_limit.check_offset_seconds', 30 * 24 * 60 * 60);
  }

}
