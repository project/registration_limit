<?php

declare(strict_types=1);

namespace Drupal\registration_limit\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\registration_limit\Mapper\LastLoginMapper;
use Drupal\user\UserDataInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Service to work with client ips.
 */
class ClientIpService {

  /**
   * Construct new instance.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected UserDataInterface $userData,
    protected RequestStack $requestStack,
    protected ConfigFactoryInterface $configFactory,
    protected LastLoginMapper $lastLoginMapper,
    #[Autowire(service: 'logger.channel.registration_limit')]
    protected LoggerInterface $logger,
  ) {}

  /**
   * Saves current request ip for given user.
   *
   * @param string|int $uid
   *   The user id.
   */
  public function saveRequestIpForUser(string|int $uid): void {
    $request = $this->requestStack->getCurrentRequest();
    $ip = $request->getClientIp();

    $this->lastLoginMapper->saveLastLoginData($uid, $ip);
  }

  /**
   * Checks recent logins and sessions for given IP address.
   *
   * @param string $ip
   *   The IP address to check for.
   */
  public function ipRecentlyUsed(string $ip): bool {
    // Check whitelist.
    if ($this->isIpWhitelisted($ip)) {
      $this->logger->info('IP Whitelisted @ip', [
        '@ip' => $ip,
      ]);

      return FALSE;
    }

    return $this->lastLoginMapper->ipRecentlyUsedForLogin($ip) ||
      $this->lastLoginMapper->ipRecentlyUsedInSession($ip);
  }

  /**
   * Checks if given ip address in whitelisted.
   *
   * @param string $ip
   *   The IP address to check.
   */
  protected function isIpWhitelisted(string $ip): bool {
    $ipWhitelist = $this->configFactory->get('registration_limit.settings')->get('ip_whitelist') ?? '';

    $ipAddresses = explode(PHP_EOL, $ipWhitelist);

    foreach ($ipAddresses as $ipAddress) {
      if (trim($ipAddress) === $ip) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
