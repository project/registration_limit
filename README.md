# Registration Limit

## Contents of this file

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Troubleshooting](#troubleshooting)
- [Maintainers](#maintainers)

## Introduction

The Registration Limit module limits/blocks registration of new user accounts
when the user's IP address was recently used in former logins to existing
accounts.

This is achieved by saving the timestamp and IP whenever a user logs
in to Drupal.

Whenever a new user wants to register for an account the IP address
of that new user is checked against previous logins, and if a match
is found and is within the configured time limit, the registration
is blocked.

Additionally certain IP Addresses can be whitelisted to circumvent
potential blocks.

### Submodules

- `registration_limit_user_api`:
  Integrates with the [User API](https://www.drupal.org/project/user_api) module
  to block registrations via this modules API.

Use the [Issue queue](https://www.drupal.org/project/issues/registration_limit)
to submit bug reports and feature suggestions, or track changes.

## Requirements

The core module requires Drupal 10 and no additional modules.

The `registration_limit_user_api` submodule requires the User API module:

- [User API](https://www.drupal.org/project/user_api)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

When using the User API contrib module, also enable the
`registration_limit_user_api` submodule.

### Configuration

Some aspects of this module can be configured:

- Whitelist of IP addresses to bypass blocked registration
- Duration of how long a user must not have logged in before a registration
  of a new account is allowed with the same IP address.

The IP whitelist can be configured in the UI at
`/admin/config/registration-limit`.

The duration can be set in `settings.php`:

```php
// Set duration to 30 days (in seconds).
$settings['registration_limit.check_offset_seconds'] = 30 * 24 * 60 * 60;
```

## Troubleshooting

The module uses raw SQL queries to write and read from the database.
This can lead to incompatibilities with certain database drivers but should
work with MySQL implementations.

Tested databases:

- sqlite
- MariaDB

## Maintainers

Current maintainers:

- Christoph Niedermoser ([@nimoatwoodway](https://www.drupal.org/u/nimoatwoodway))
- Christian Foidl ([@chfoidl](https://www.drupal.org/u/chfoidl))
