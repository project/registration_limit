<?php

declare(strict_types=1);

namespace Drupal\registration_limit_user_api\EventSubscriber;

use Drupal\Core\Site\Settings;
use Drupal\registration_limit\Service\ClientIpService;
use Drupal\user_api\Event\AdvancedUserRegistrationEvent;
use Drupal\user_api\UserApiEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Listen on advanced user registration events.
 */
class AdvancedUserRegistrationEventSubscriber implements EventSubscriberInterface {

  /**
   * Construct new instance.
   */
  public function __construct(
    protected RequestStack $requestStack,
    protected ClientIpService $clientIpService,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      UserApiEvents::ADVANCED_USER_REGISTRATION => 'onRegistration',
    ];
  }

  /**
   * Check if registration is limited and abort if necessary.
   */
  public function onRegistration(AdvancedUserRegistrationEvent $event) {
    $ip = $this->requestStack->getCurrentRequest()->getClientIp();

    $isEnabled = Settings::get('registration_limit.enabled', TRUE);

    if ($isEnabled && $this->clientIpService->ipRecentlyUsed($ip)) {
      $event->abort(
        'other_account_used_on_ip',
        'Another active account is already being used by your IP address.',
        Response::HTTP_FORBIDDEN
      );
    }
  }

}
