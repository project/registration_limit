<?php

declare(strict_types=1);

namespace Drupal\Tests\registration_limi_user_api\Kernel;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\registration_limit\Service\ClientIpService;
use Drupal\rest\Entity\RestResourceConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\user_api\Kernel\UserApiTestTrait;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Test registration block in user api.
 */
class UserApiRegistrationBlockTest extends BrowserTestBase {

  use UserApiTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'rest',
    'serialization',
    'user',
    'user_api',
    'registration_limit',
    'registration_limit_user_api',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The http kernel.
   */
  protected HttpKernelInterface $httpKernel;

  /**
   * Rest endpoint url.
   */
  protected Url $url;

  /**
   * The client ip service.
   */
  protected ClientIpService $clientIpService;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    RestResourceConfig::create([
      'id' => 'user_api_user_registration',
      'plugin_id' => 'user_api_user_registration',
      'granularity' => RestResourceConfig::RESOURCE_GRANULARITY,
      'configuration' => [
        'methods' => ['POST'],
        'formats' => ['json'],
        'authentication' => ['cookie'],
      ],
    ])->save();

    // Write something via the session handler
    // to have the `session` table created.
    $this->container->get('session_handler.storage')->write('sess_test', 'test');
    $this->rebuildAll();

    $this->setCurrentUser(User::getAnonymousUser());
    $this->grantPermissions(Role::load(Role::ANONYMOUS_ID), ['restful post user_api_user_registration']);

    $settings = $this->config('user.settings');
    $settings
      ->set('register', UserInterface::REGISTER_VISITORS)
      ->set('verify_mail', TRUE)
      ->save();

    $this->url = Url::fromRoute('rest.user_api_user_registration.POST');
    $this->httpKernel = $this->container->get('http_kernel');
    $this->clientIpService = $this->container->get('registration_limit.client_ip_service');
  }

  /**
   * Test registration is blocked when using user api.
   */
  public function testRegistrationBlock(): void {
    // No block.
    $username = 'test_user_' . time();

    $content = [
      'name' => [
        'value' => $username,
      ],
      'mail' => [
        'value' => $username . '@example.com',
      ],
    ];

    $request = $this->createJsonRequest('POST', $this->url->toString(), $content);
    $response = $this->httpKernel->handle($request);
    $this->assertValidRegistrationResponse($response);
    $this->assertUser($username, TRUE, TRUE);

    $userResult = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties([
      'name' => $username,
    ]);
    $user = reset($userResult);

    // User logged in.
    $this->clientIpService->saveRequestIpForUser($user->id());

    // Register again.
    $username = 'test_user2_' . time();

    $content = [
      'name' => [
        'value' => $username,
      ],
      'mail' => [
        'value' => $username . '@example.com',
      ],
    ];

    $request = $this->createJsonRequest('POST', $this->url->toString(), $content);
    $response = $this->httpKernel->handle($request);
    $this->assertEquals(403, $response->getStatusCode());
    $this->assertStringContainsString('other_account_used_on_ip', $response->getContent());
  }

  /**
   * Asserts that the response is a valid registration response.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response.
   */
  protected function assertValidRegistrationResponse(Response $response) {
    $this->assertEquals(200, $response->getStatusCode());
    $parsedResponse = Json::decode($response->getContent());

    $this->assertArrayHasKey('uid', $parsedResponse);
    $this->assertArrayHasKey('name', $parsedResponse);
  }

  /**
   * Asserts that a user with the given username exists.
   *
   * @param string $username
   *   The username.
   * @param bool $exists
   *   If user should exist.
   * @param bool $status
   *   If user should be active.
   */
  protected function assertUser(string $username, bool $exists, bool $status) {
    $result = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties(['name' => $username]);

    if (empty($result)) {
      $this->assertFalse($exists, sprintf('User with username %s does not exist!', $username));
      return;
    }

    $this->assertTrue($exists, sprintf('User with username %s exists!', $username));
    $this->assertEquals($status, reset($result)->isActive(), sprintf('User with username %s has status %s!', $username, $status));
  }

}
