<?php

declare(strict_types=1);

namespace Drupal\Tests\registration_limit\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Test registration_limit on register from.
 */
class RegisterFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'registration_limit',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $config = $this->config('user.settings');
    // Don't require email verification and allow registration by site visitors
    // without administrator approval.
    $config
      ->set('verify_mail', FALSE)
      ->set('register', UserInterface::REGISTER_VISITORS)
      ->save();

    // Write something via the session handler
    // to have the `session` table created.
    $this->container->get('session_handler.storage')->write('sess_test', 'test');
  }

  /**
   * Test registration block.
   */
  public function testRegistrationBlock() {
    $this->writeSettings([
      'settings' => [
        'registration_limit.check_offset_seconds' => (object) [
          'value' => 3,
          'required' => TRUE,
        ],
      ],
    ]);

    // Register new account; is permitted, due to no prior login.
    $this->drupalGet('user/register');

    $this->submitForm([
      'mail' => 'test@wunderwerk.io',
      'name' => 'test',
      'pass[pass1]' => 'abc123',
      'pass[pass2]' => 'abc123',
    ], 'Create new account');

    // User is logged in.
    $this->assertSession()->pageTextContains('Member for');

    $result = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties([
      'mail' => 'test@wunderwerk.io',
    ]);

    $this->assertCount(1, $result);

    // Logout.
    $this->drupalLogout();

    // Register new account is blocked, because within check_offset_seconds.
    $this->drupalGet('user/register');
    $this->submitForm([
      'mail' => 'test+2@wunderwerk.io',
      'name' => 'test2',
      'pass[pass1]' => 'abc123',
      'pass[pass2]' => 'abc123',
    ], 'Create new account');

    $this->assertSession()->pageTextContains('registration_limit.error_message');

    /** @var \Drupal\Core\Session\SessionManagerInterface $sessionManager */
    $sessionManager = $this->container->get('session_manager');
    $sessionManager->delete(2);

    sleep(4);

    // Register again after expiry.
    $this->submitForm([
      'mail' => 'test+2@wunderwerk.io',
      'name' => 'test2',
      'pass[pass1]' => 'abc123',
      'pass[pass2]' => 'abc123',
    ], 'Create new account');

    // User is logged in.
    $this->assertSession()->pageTextContains('Member for');
  }

}
